-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 25 jan. 2021 à 20:35
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pjencheredb`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

DROP TABLE IF EXISTS `annonce`;
CREATE TABLE IF NOT EXISTS `annonce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F65593E560BB6FE6` (`auteur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`id`, `auteur_id`, `title`, `content`, `image`, `created_at`) VALUES
(1, 7, 'titre1', 'contenu test', 'https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png', '2021-01-25 10:13:57'),
(3, 9, 'titre2', 'test de contenu ', 'https://upload.wikimedia.org/wikipedia/commons/1/1c/Image_du_Maroc_3.jpg', '2021-01-25 10:26:35'),
(6, 9, 'annoncefinale', 'aa', 'zzaa', '2021-01-25 14:23:58'),
(7, 9, 'annoncetest3', 'zeezr', 'eeze', '2021-01-25 14:57:25'),
(8, 9, 'annoncetest3', 'zeezr', 'eeze', '2021-01-25 15:31:58'),
(9, 9, 'annoncetest3', 'zeezr', 'eeze', '2021-01-25 15:32:01'),
(10, 9, 'annoncesoir', 'le soir', 'aaa', '2021-01-25 18:06:03'),
(11, 9, 'annonce', 'zzz', 'aaa', '2021-01-25 20:27:50');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210121095746', '2021-01-21 09:58:41', 39),
('DoctrineMigrations\\Version20210125085318', '2021-01-25 08:59:56', 265),
('DoctrineMigrations\\Version20210125085907', '2021-01-25 09:04:11', 664);

-- --------------------------------------------------------

--
-- Structure de la table `enchere`
--

DROP TABLE IF EXISTS `enchere`;
CREATE TABLE IF NOT EXISTS `enchere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annonce_id` int(11) DEFAULT NULL,
  `prix_immediat` int(11) DEFAULT NULL,
  `prix_depart` int(11) DEFAULT NULL,
  `bid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_38D1870F8805AB2F` (`annonce_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `enchere`
--

INSERT INTO `enchere` (`id`, `annonce_id`, `prix_immediat`, `prix_depart`, `bid`) VALUES
(2, 9, 6, 4, NULL),
(3, 10, 2, 7, NULL),
(4, 11, 12, -4, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`) VALUES
(7, 'pierre.b221@gmail.com', 'pa', '$2y$13$Mb1URwLqwW3WXfIoc/vVj.Wi8yfFHXD3DgMY4DyihWeHs3uP5HSTW'),
(8, 'piaaerre.b221@gmail.com', 'pa', '$2y$13$s1RaJM8K/mzEKX9VwQVWie1NUG.a.qP.UcwWkMkvztcOi6TgO8YKi'),
(9, 'pap@gmail.com', 'pa', '$2y$13$3n4CdQRhNNtwfYuwplBiY.1tWW.3bMqxz98jPI7H2QRRKuOs4s3mC');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `FK_F65593E560BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `enchere`
--
ALTER TABLE `enchere`
  ADD CONSTRAINT `FK_38D1870F8805AB2F` FOREIGN KEY (`annonce_id`) REFERENCES `annonce` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
