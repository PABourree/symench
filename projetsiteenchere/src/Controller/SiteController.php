<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Enchere;

class SiteController extends AbstractController
{
    /**
     * @Route("/site", name="site")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonces = $repo->findAll();
        
        $repo = $this->getDoctrine()->getRepository(Enchere::class);
        $encheres = $repo->findAll();

        dump($annonces);
        dump($encheres);

        return $this->render('site/index.html.twig', [
            'controller_name' => 'SiteController',
            'annonces' => $annonces,
            'encheres' => $encheres
        ]);
    }
    /**
     * @Route("/site/tableau", name="site_tableau")
     */
    public function tableau() {
        
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonces = $repo->findAll();
        
        $repo = $this->getDoctrine()->getRepository(Enchere::class);
        $encheres = $repo->findAll();

        $user = $this->getUser();

        dump($annonces);
        dump($encheres);

        return $this->render('site/tableau.html.twig', [
            'controller_name' => 'SiteController',
            'annonces' => $annonces,
            'encheres' => $encheres,
            'user' => $user
        ]);

    }

    /**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('site/home.html.twig');
    }

    /**
     * @Route("/site/new", name="site_create")
     */
    public function create(Request $request, EntityManagerInterface  $manager){
        $annonce = new Annonce();

        $user = $this->getUser();
        dump($user);

        $form = $this->createFormBuilder($annonce)
                     ->add('title')
                     ->add('content')
                     ->add('image')
                     ->add('save', SubmitType::class, [
                         'label' => 'Suivant'
                     ])
                     ->getForm();

                     $form->handleRequest($request);

                     if($form->isSubmitted() && $form->isValid()){
                         $annonce->setCreatedAt(new \DateTime());
                         $annonce->setAuteur($user);
                         $manager->persist($annonce);
                         $manager->flush();
                     return $this->redirectToRoute('site_createenchere', [
                            'id' => $annonce->getId()
                        ]);
                     }
            return $this->render('site/create.html.twig',[
                'formAnnonce' => $form->createView()
            ]);
    }
    /**
     * @Route("/site/newenchere{id}", name="site_createenchere")
     */
    public function createenchere($id, Request $request, EntityManagerInterface  $manager){
        
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonce = $repo->find($id);
        
        $enchere = new Enchere();


        $form = $this->createFormBuilder($enchere)
                     ->add('prixImmediat')
                     ->add('prixDepart')
                     ->add('save', SubmitType::class, [
                        'label' => 'Suivant'
                    ])
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $enchere->setAnnonce($annonce);
            $manager->persist($enchere);
            $manager->flush();
        }


        return $this->render('site/createenchere.html.twig', [
            'formEnchere' => $form->createView()
        ]);
    }

    /**
     * @Route("/site/{id}", name="site_show")
     */
    public function show($id){
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonce = $repo->find($id);
        return $this->render('site/show.html.twig',[
            'annonce' => $annonce
        ]);
    }
    
}
