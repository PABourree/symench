<?php

namespace App\Entity;

use App\Repository\EnchereRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EnchereRepository::class)
 */
class Enchere
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prixImmediat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prixDepart;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bid;

    /**
     * @ORM\ManyToOne(targetEntity=Annonce::class, inversedBy="encheres")
     */
    private $annonce;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrixImmediat(): ?int
    {
        return $this->prixImmediat;
    }

    public function setPrixImmediat(?int $prixImmediat): self
    {
        $this->prixImmediat = $prixImmediat;

        return $this;
    }

    public function getPrixDepart(): ?int
    {
        return $this->prixDepart;
    }

    public function setPrixDepart(?int $prixDepart): self
    {
        $this->prixDepart = $prixDepart;

        return $this;
    }

    public function getBid(): ?int
    {
        return $this->bid;
    }

    public function setBid(?int $bid): self
    {
        $this->bid = $bid;

        return $this;
    }

    public function getAnnonce(): ?Annonce
    {
        return $this->annonce;
    }

    public function setAnnonce(?Annonce $annonce): self
    {
        $this->annonce = $annonce;

        return $this;
    }
}
